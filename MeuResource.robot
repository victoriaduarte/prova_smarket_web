*** Settings ***
Library       SeleniumLibrary

*** Variables ***
${URL}        http://automationpractice.com/index.php
${BROWSER}    chrome

*** Keywords ***
Acessar a página
    Open browser                     ${URL}                                       browser=${BROWSER}
    Maximize browser window
    Title Should Be                  My Store

Passar o mouse por cima da categoria "Dresses" no menu principal superior de categorias
    Mouse over                       //*[@id="block_top_menu"]/ul/li[2]/a
    Element Should Be Visible        //*[@id="block_top_menu"]/ul/li[2]/ul/li[1]/a
    Element Should Be Visible        //*[@id="block_top_menu"]/ul/li[2]/ul/li[2]/a
    Element Should Be Visible        //*[@id="block_top_menu"]/ul/li[2]/ul/li[3]/a

Clicar na subcategoria "Summer Dresses"
    Wait until Element is enabled    //*[@id="block_top_menu"]/ul/li[2]/ul/li[3]/a
    Mouse over                       //*[@id="block_top_menu"]/ul/li[2]/ul/li[3]/a
    Click Element                    //*[@id="block_top_menu"]/ul/li[2]/ul/li[3]/a
    Page Should Contain Element      //div[@class="product-container"]
    Sleep                            5

Fechar o navegador
    Close browser

